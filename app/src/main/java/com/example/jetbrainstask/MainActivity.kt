package com.example.jetbrainstask

import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView


class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		val x = event.x
		val y = event.y
		when (event.action) {
			MotionEvent.ACTION_DOWN  -> {

				val displayMetrics = DisplayMetrics()
				windowManager.defaultDisplay.getMetrics(displayMetrics)
				val height = displayMetrics.heightPixels
				val width = displayMetrics.widthPixels
				val circleImageView: ImageView = findViewById<View>(R.id.circle) as ImageView
				val newX = (x-width/2)
				val newY = (y-height/2-80) // вычетаем экран чтобы не было проблемы с началом координат и отнимаем размер верха
 				Log.e("TAG", newY.toString())

				ObjectAnimator.ofFloat(circleImageView, View.TRANSLATION_X, newX).start()
				ObjectAnimator.ofFloat(circleImageView, View.TRANSLATION_Y, newY).start()
			}
		}
		return false
	}
}
